package booter;

import com.baomidou.mybatisplus.annotations.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.ConfigGenerator;

/**
 * mybatis生成代码类
 * 配置路径和数据库连接
 * @author ZhangMH
 *
 */
public class CodeGenarator extends ConfigGenerator {

	public  static  void  main(String args []){
		
		ConfigGenerator cf = new ConfigGenerator();
		
		/**
		 * 设置数据库连接信息
		 */
		cf.setDbDriverName("com.mysql.jdbc.Driver");
		cf.setDbUser("root");
		cf.setDbPassword("");
		cf.setDbUrl("jdbc:mysql://127.0.0.1:3306/boot?characterEncoding=utf8");
		
		/**
		 * 设置包路径
		 */
		cf.setEntityPackage("com.fms.bean");
		cf.setMapperPackage("com.fms.mapper");
		cf.setXmlPackage("com.fms.xml");
		cf.setServicePackage("com.fms.service");
		cf.setServiceImplPackage("com.fms.service.impl");
		/**
		 * 生成文件保存的目标路径
		 */
		cf.setSaveDir("D:/code/workspace/boot/src/main/java");
		
		/**
		 * 是否标注数据库字段
		 */
		cf.setDbColumnUnderline(true);
		
		/**
		 * 生成类是否带数据库前缀
		 */
		cf.setDbPrefix(false);
		
		/**
		 * 是否启用文件覆盖
		 */
		cf.setFileOverride(true);
		
		/**
		 * 设置数据id为uuid
		 */
		cf.setIdType(IdType.UUID);
		
		/**
		 * 自定义生成的类名
		 */
		
		cf.setMapperName("%Mapper");
		cf.setServiceName("%Service");
		cf.setServiceImplName("%ServiceImpl");
		
		
		/*
		 * 指定生成表名（默认，所有表）
		 */
		//cg.setTableNames(new String[]{"user"});

		
		
		AutoGenerator.run(cf);
	}
	
}
