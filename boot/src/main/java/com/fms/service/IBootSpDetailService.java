package com.fms.service;

import com.fms.bean.BootSpDetail;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * BootSpDetail 表数据服务层接口
 *
 */
public interface IBootSpDetailService extends ISuperService<BootSpDetail> {


}