package com.fms.service;

import com.fms.bean.BootSpBase;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * BootSpBase 表数据服务层接口
 *
 */
public interface IBootSpBaseService extends ISuperService<BootSpBase> {


}