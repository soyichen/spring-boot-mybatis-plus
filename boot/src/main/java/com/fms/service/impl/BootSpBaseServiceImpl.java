package com.fms.service.impl;

import org.springframework.stereotype.Service;

import com.fms.mapper.BootSpBaseMapper;
import com.fms.bean.BootSpBase;
import com.fms.service.IBootSpBaseService;
import com.baomidou.framework.service.impl.SuperServiceImpl;

/**
 *
 * BootSpBase 表数据服务层接口实现类
 *
 */
@Service
public class BootSpBaseServiceImpl extends SuperServiceImpl<BootSpBaseMapper, BootSpBase> implements IBootSpBaseService {


}