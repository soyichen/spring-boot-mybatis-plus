package com.fms.service.impl;

import org.springframework.stereotype.Service;

import com.fms.mapper.BootSpDetailMapper;
import com.fms.bean.BootSpDetail;
import com.fms.service.IBootSpDetailService;
import com.baomidou.framework.service.impl.SuperServiceImpl;

/**
 *
 * BootSpDetail 表数据服务层接口实现类
 *
 */
@Service
public class BootSpDetailServiceImpl extends SuperServiceImpl<BootSpDetailMapper, BootSpDetail> implements IBootSpDetailService {


}