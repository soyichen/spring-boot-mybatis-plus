package com.fms.mapper;

import com.fms.bean.BootSpBase;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * BootSpBase 表数据库控制层接口
 *
 */
public interface BootSpBaseMapper extends AutoMapper<BootSpBase> {


}