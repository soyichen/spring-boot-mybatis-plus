package com.fms.mapper;

import com.fms.bean.BootSpDetail;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * BootSpDetail 表数据库控制层接口
 *
 */
public interface BootSpDetailMapper extends AutoMapper<BootSpDetail> {


}